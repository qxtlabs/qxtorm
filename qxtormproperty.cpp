/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#include "qxtormproperty.h"

QxtOrmProperty::QxtOrmProperty( const char *name ) : m_id(false),m_stored(true), m_name(name)
{
}

bool QxtOrmProperty::isId() const
{
    return m_id;
}

void QxtOrmProperty::setId(const bool set)
{
    m_id = set;
}

bool QxtOrmProperty::isStored() const
{
    return m_stored;
}

void QxtOrmProperty::setStored(const bool set)
{
    m_stored = set;
}

QByteArray QxtOrmProperty::name() const
{
    return m_name;
}

void QxtOrmProperty::setName(const char *name)
{
    m_name = name;
}

QString QxtOrmProperty::columnName() const
{
    if(m_columnName.isEmpty())
        return m_name;
    return m_columnName;
}

void QxtOrmProperty::setColumnName(const QString &name)
{
    m_columnName = name;
}

QVariant QxtOrmProperty::nullValue() const
{
    return m_nullValue;
}

int QxtOrmProperty::metaTypeId() const
{
    return m_typeId;
}
