/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#ifndef QXTORMTYPE_H
#define QXTORMTYPE_H

#include "qxtorm_global.h"
#include <QMap>
#include <QMetaType>
#include <QList>
#include <QSharedPointer>
#include "qxtormrelation.h"

class QxtOrmTypePrivate;
class QxtOrmTypeBuilder;
class QxtOrmProperty;
class QxtOrmObject;


class QXT_ORM_EXPORT QxtOrmType
{
    friend class QxtOrmTypeBuilder;
    public:
        QxtOrmType(QxtOrmTypePrivate* dPtr);
        QxtOrmType(const QxtOrmType &other);
        ~QxtOrmType();

        QxtOrmType& operator=(const QxtOrmType &other);

        bool        hasProperty(const char *name) const;
        bool        writeProperty( QxtOrmObject* o, const char* name, const QVariant &value ) const;
        QVariant    readProperty( const QxtOrmObject* o,const char* name ) const;

        const QxtOrmProperty&   id () const;
        const QxtOrmProperty&   property( const char* name ) const;
        const QxtOrmProperty*   searchProperty( const char* name ) const;
        QList<QByteArray>       properties () const;

        QList<QByteArray>       relations ( ) const;
        QList<QByteArray>       relations ( QxtOrmRelation::Type type ) const;
        const QxtOrmRelation&   relation  ( const char* name ) const;

        const QxtOrmType*       superClass () const;
        const QxtOrmType*       baseClass () const;
        QList<const QxtOrmType*>inheritanceList () const;
        const QByteArray&       name () const;
        const QByteArray metaTypeName() const;
        const QString&          tableName() const;
        int                     metaTypeId ( ) const;
        bool                    isIdValid ( const QVariant &id ) const;
private:
        QSharedPointer<QxtOrmTypePrivate> d_ptr;
};
#endif // QXTORMTYPE_H
