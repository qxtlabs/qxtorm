/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#include "qxtormmanager.h"
#include "qxtormobject.h"
#include "qxtormtype.h"
#include "qxtormproperty.h"
#include "qxtormrelation.h"
#include "qxtormtransaction.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlError>
#include <QStack>
#include <QPair>
#include <QDebug>

#if 0
struct QxtOrmKey{

    QxtOrmKey(const QxtOrmProperty* p , QVariant v) : prop(p),value(v){}

    uint doHash() const{return prop->qHash(value);}
    const QxtOrmProperty* prop;
    QVariant value;
};

uint qHash(const QxtOrmKey &key){
    return key.doHash();
}
typedef QHash<QxtOrmKey, QWeakPointer<QxtOrmObject*> >  ObjectCache;
#endif

class QxtOrmManagerPrivate{
    public:
        QStack<QxtOrmTransaction*> transactions;
        //QMap<quintptr,ObjectCache> existingObjects;
        QSqlDatabase connection;
        QSqlError    lastError;
};

QxtOrmManager::QxtOrmManager(QSqlDatabase connection)
{
    d_ptr = new QxtOrmManagerPrivate;
    d_ptr->connection = connection;
}

QxtOrmManager::~QxtOrmManager()
{
    if(d_ptr->transactions.size()){
        qWarning()<<"WARNING PENDING TRANSACTIONS IN ORMMANAGER";
        qDeleteAll(d_ptr->transactions.begin(),d_ptr->transactions.end());
    }
    delete d_ptr;
}

bool QxtOrmManager::saveObject(QxtOrmObject *obj)
{
    QList<const QxtOrmType*> inheritanceList =  obj->typeInfo()->inheritanceList();
    const QxtOrmType* baseClass = inheritanceList.first();

    QVariant oldId = baseClass->readProperty(obj,baseClass->id().name());
    QVariant id    = oldId;
    bool isUpdate = baseClass->isIdValid(oldId);

    if(!transaction())
        return false;

    foreach(const QxtOrmType* t,inheritanceList){

        QString sql;
        const QxtOrmProperty& idProperty = t->id();

        //only needed for INSERT
        QString valuePart;
        QString namePart;

        if(isUpdate)
            sql = QString("UPDATE ")+t->tableName()+QLatin1String(" SET ");
        else
            sql = QString("INSERT INTO ")+t->tableName()+QLatin1String(" (");

        bool firstRun = true;
        QList<QByteArray> props = t->properties();
        QVariantList values;
        foreach(const QByteArray cProp, props){
            const QxtOrmProperty& prop = t->property(cProp);
            if(!prop.isStored())
                continue;
            if(prop.isId())
                continue;

            if(isUpdate){
                if(!firstRun)
                    sql.append(",");
                sql.append(prop.columnName()+" = ?");
            }else{
                if(!firstRun){
                    namePart.append(",");
                    valuePart.append(",");
                }
                namePart.append(prop.columnName());
                valuePart.append("?");
            }
            firstRun = false;
            values.append(prop.read(obj));
        }


#if 0
        QList<QByteArray> relations = t->relations();
        foreach(const QByteArray &rel,relations){
            const QxtOrmRelation& relation = t->relation(rel);

            //we only handle one to one and many to one relations here because we need their ids in our table
            if(relation.relationType() != QxtOrmRelation::OneToOne
              ||relation.relationType() != QxtOrmRelation::ManyToOne)
                continue;

            for(int i = 0; i < relation.count(obj);i++){
                QxtOrmObject* subObj = relation.at(obj,i);
                //TODO include check for isDirty and doCascade
                saveObject(subObj);
                const QxtOrmType* baseClassInfo = subObj->typeInfo()->baseClass();
                QVariant subId = baseClassInfo->id().read(subObj);
                if(!baseClassInfo->isIdValid(subId)){
                    rollback();
                    return false;
                }

                if(isUpdate){
                    if(!firstRun)
                        sql.append(",");
                    sql.append(relation.foreignKey()+" = ?");
                }else{
                    if(!firstRun){
                        namePart.append(",");
                        valuePart.append(",");

                        namePart.append(relation.foreignKey());
                        valuePart.append("?");
                    }
                }
                firstRun = false;
                values.append(subId);
            }
        }
#endif

        if(isUpdate){
            sql.append(" WHERE "+idProperty.columnName()+" = ?;");
            values.append(id);
        }else{
            if(t!=baseClass){
                if(!firstRun){
                    //after we inserted the baseclass we reuse its id in all subclass tables
                    namePart.append(",");
                    valuePart.append(",");
                }
                namePart.append(idProperty.columnName());
                valuePart.append("?");
                values.append(id);
            }
            sql.append(namePart+QLatin1String(") VALUES ( ")+valuePart+QLatin1String(" ) RETURNING ")+idProperty.columnName()+QLatin1String(";"));
        }

        qDebug()<<"Using connection: "<<d_ptr->connection.connectionName();
        QSqlQuery q(d_ptr->connection);
        q.prepare(sql);
        foreach(const QVariant& v, values)
            q.addBindValue(v);

        if(!q.exec()){
            QSqlError err = q.lastError();
            qDebug()<<"Database Error: "<<err.databaseText();
            qDebug()<<"Driver Text: "<<err.driverText();
            qDebug()<<"Executed: "<<q.executedQuery();
            rollback();
            return false;
        }

        if(!isUpdate){
            if(t == baseClass){
                if(!q.first()){
                    //we need the id, if we don't get a record we have a problem
                    rollback();
                    return false;
                }
                id = q.value(0);
            }

            d_ptr->transactions.top()->addOperation(new QxtOrmSetValueOperation(idProperty.name(),t,obj,oldId));
            idProperty.write(obj,id);
        }

#if 0
        //Now we need to handle all the other relations
        foreach(const QByteArray &rel,relations){
            const QxtOrmRelation& relation = t->relation(rel);
            QxtOrmRelation::Type relType = relation.relationType();

            switch(relType){
                case QxtOrmRelation::OneToOne:
                    //we already handled one to one relations
                    continue;
                break;
                case QxtOrmRelation::OneToMany:
                break;
            }
        }
#endif

    }
    bool result = commit();
    if(!result)
        qDebug()<<"Could not Store the Data because commit failed: "<<d_ptr->connection.lastError();
    return result;
}

bool QxtOrmManager::unlinkObject(QxtOrmObject* obj)
{
    if(!this->transaction())
        return false;
    QList<const QxtOrmType*> inheritanceList =  obj->typeInfo()->inheritanceList();

    foreach(const QxtOrmType* t,inheritanceList){
        QSqlQuery del(d_ptr->connection);
        del.prepare("DELETE from "+t->tableName()+" WHERE "+t->id().columnName()+" = :id;");
        del.bindValue(":id",t->id().read(obj));
        if(!del.exec()){
            qDebug()<<"Delete did not happen: "<<del.lastError().databaseText()+" "+del.lastError().driverText();
            rollback();
            return false;
        }
    }
    return true;
}

bool QxtOrmManager::transaction()
{
    if(d_ptr->transactions.size()){
        //PGSQL does not support nested transactions
        QSqlQuery q(d_ptr->connection);
        QUuid savepointName = QUuid::createUuid();
        if(!q.exec("SAVEPOINT \""+savepointName.toString()+"\";")){
            qDebug()<<"ROLLBACK SAVEPOINT failed: "<<q.lastError().driverText()<<" "<<q.lastError().databaseText();
            return false;
        }
        d_ptr->transactions.push(new QxtOrmTransaction(savepointName));
    }else{
        if(!d_ptr->connection.transaction()){
            qDebug()<<"Could not start Transaction "<<d_ptr->connection.lastError();
            return false;
        }
        d_ptr->transactions.push(new QxtOrmTransaction());
    }
    return true;
}

bool QxtOrmManager::rollback()
{
    if(!d_ptr->transactions.size())
        return true;

    bool success = true;
    QxtOrmTransaction *t = d_ptr->transactions.top();
    if(t->isSavepoint()){
        QSqlQuery q(d_ptr->connection);
        if(!q.exec("ROLLBACK TO SAVEPOINT \""+t->savepointName().toString()+"\";")){
            qDebug()<<"ROLLBACK SAVEPOINT failed: "<<q.lastError().driverText()<<" "<<q.lastError().databaseText();
            return false;
        }
    }else{
        success = d_ptr->connection.rollback();
    }

    //@BUG delete Transaction data also when rollback does not work?
    if(success){
        success = t->rollback();
        d_ptr->transactions.pop();
        delete t;
    }

    return success;
}

bool QxtOrmManager::commit()
{
    if(!d_ptr->transactions.size())
        return true;
    QxtOrmTransaction *t = d_ptr->transactions.top();
    if(t->isSavepoint()){
        QSqlQuery q(d_ptr->connection);
        if(!q.exec("RELEASE SAVEPOINT \""+t->savepointName().toString()+"\";")){
            qDebug()<<"RELEASE SAVEPOINT failed: "<<q.lastError().driverText()<<" "<<q.lastError().databaseText();
            return false;
        }
    }else{
        if(!d_ptr->connection.commit())
            return false;
    }
    //finally remove the transaction from the stack
    d_ptr->transactions.pop();

    if(d_ptr->transactions.size())
        d_ptr->transactions.top()->addOperationsFromNestedTransaction(t);

    delete t;
    return true;
}

QSqlDatabase QxtOrmManager::connection()
{
    return d_ptr->connection;
}

QString QxtOrmManager::createSelect(const QxtOrmType *typeInformation)
{
    QString select = "SELECT ";
    return appendFieldNames(typeInformation,select);
}

QString &QxtOrmManager::appendFieldNames(const QxtOrmType *typeInformation, QString &query)
{
    QList<const QxtOrmType*> inheritanceList = typeInformation->inheritanceList();
    bool firstRun = true;

    QString &select = query;
    foreach(const QxtOrmType* curr, inheritanceList){
        QList<QByteArray> fieldNames = curr->properties();
        foreach(const QByteArray& f, fieldNames){
            if(!firstRun)
                select.append(",");
            const QxtOrmProperty& p = curr->property(f);
            select.append(curr->tableName()+"."+p.columnName());

            firstRun = false;
        }
    }
    query.append(" ");

    return query;
}

QString &QxtOrmManager::appendFromPart(const QxtOrmType *typeInformation, QString &query)
{
    QList<const QxtOrmType*> inheritanceList = typeInformation->inheritanceList();
    bool firstRun = true;

    query.append(" from ");

    foreach(const QxtOrmType* curr, inheritanceList){
        if(!firstRun)
            query.append(",");
        query.append(curr->tableName());

        firstRun = false;
    }
    query.append(" ");
    return query;
}

QSqlQuery QxtOrmManager::createQuery(const QxtOrmType *typeInformation, const QString &where, const QVariantMap &values)
{

    QList<const QxtOrmType*> inheritanceList = typeInformation->inheritanceList();

    //for now we assume that every subclass is referenced by the id column and has no own id col.
    //that means a subclass table has no auto increment key but instead uses the same as the baseclass' table
    //insert code also will assume exactly the same
    //or more easily if base has id=1 child will also have id=1
    bool firstRun = true;

    //TODO insert fields into select and rename them to the property  Select field as Person.property

    QString select("Select * from ");
    foreach(const QxtOrmType* curr, inheritanceList){
        if(!firstRun)
            select.append(",");
        select.append(curr->tableName()+" as "+curr->name());

#if 0
        QList<QByteArray> fieldNames = curr->properties();
        foreach(const QByteArray& f, fieldNames){
            if(!firstRun)
                fields.append(",");
            const QxtOrmProperty& p = curr->property(f);
            fields.append(curr->tableName()+"."+p.columnName());
        }
#endif
        firstRun = false;

    }
    if(inheritanceList.size() > 1 || where.size() > 0)
        select.append(" WHERE ");

    firstRun = true;
    if(inheritanceList.size() > 1){
        const QxtOrmType* baseClassType = inheritanceList.first();
        foreach(const QxtOrmType* curr, inheritanceList){
            if(!firstRun)
                select.append(" AND ");
            select.append(baseClassType->name()+"."+baseClassType->id().columnName()+" = "
                          +curr->name()+"."+curr->id().columnName());
            firstRun = false;
        }
        if(!where.isEmpty())
            select.append(" AND ");
    }

    if(!where.isEmpty())
        select.append(" ("+where+")");
    select.append(";");

    QSqlQuery q(d_ptr->connection);
    q.prepare(select);
    QVariantMap::const_iterator i = values.constBegin();
    while (i != values.constEnd()) {
        q.bindValue(i.key(),i.value());
        i++;
    }

    if(q.exec())
        return q;

    qDebug()<<q.executedQuery();
    qDebug()<<q.lastError().databaseText()<<" "<<q.lastError().driverText();
    return QSqlQuery();
}

QxtOrmObject* QxtOrmManager::mapToObject(const QxtOrmType *typeInformation, const QSqlQuery &sel, const QSqlRecord &rec )
{
    QxtOrmObject* o = reinterpret_cast<QxtOrmObject*>(QMetaType::construct(typeInformation->metaTypeId()));

    //loop through all superclasses and write the properties and relations
    const QxtOrmType* currClass = typeInformation;
    while(currClass != QxtOrmObject::staticTypeInfo()){
        foreach(const QByteArray &curr,currClass->properties()){
            const QxtOrmProperty& prop = currClass->property(curr);
            int index = rec.indexOf(prop.columnName());
            if(index < 0){
                qDebug()<<"ZOOOOOOOOONK NO COL FOUND "<<prop.columnName();
                return o;
            }
            currClass->writeProperty(o,curr.data(),sel.value(index));
        }

#if 0
        //now we need to resolv the relations
        foreach(const QByteArray &curr,currClass->relations()){
            const QxtOrmRelation& rel = currClass->relation(curr);
            switch(rel.relationType()){
            case QxtOrmRelation::OneToOne:{
                const QxtOrmType *subTypeInfo = rel.rightTypeInfo();
                QxtOrmObject * subData = objectById(subTypeInfo,sel.value(rec.indexOf(rel.columnName())).toInt());
                rel.append(o,subData);
            }break;
            case QxtOrmRelation::OneToMany:{
                QSqlQuery q;
                q.prepare(QLatin1String("SELECT * from ")
                          +rel.rightTypeInfo()->tableName()
                          +QLatin1String(" where ")+rel.foreignKey()
                          +QLatin1String("=:foreignKey;"));
                q.bindValue(":foreignKey",sel.value(rec.indexOf(currClass->id().columnName())));
                if(q.exec()){
                    QSqlRecord rec = sel.record();
                    while(q.next())
                        rel.append(o,mapToObject(rel.rightTypeInfo(),q,rec));
                }
            }break;
            case QxtOrmRelation::ManyToMany:{
                QSqlQuery q;
                q.prepare(QLatin1String("Select * from ")
                          +rel.rightTypeInfo()->tableName()
                          +QLatin1String(" where ")
                          +currClass->id().columnName()
                          +QLatin1String(" in (Select ")
                          +rel.linkForeignKey()
                          +QLatin1String(" from ")
                          +rel.linkTable()
                          +QLatin1String(" where ")
                          +rel.linkKey()
                          +QLatin1String("=:foreignKey);"));
                q.bindValue(":foreignKey",sel.value(rec.indexOf(currClass->id().columnName())));
                if(q.exec()){
                    QSqlRecord tmpRec = q.record();
                    while(q.next())
                        rel.append(o,mapToObject(rel.rightTypeInfo(),q,tmpRec));
                }
            }break;
            }
        }
#endif
        currClass = currClass->superClass();
    }

    return o;
}

QxtOrmObject* QxtOrmManager::objectById(const QxtOrmType *typeInformation, const int id)
{
    QString whereClause = typeInformation->name()+"."+typeInformation->id().columnName()+"=:id";
    QVariantMap map;
    map.insert(":id",id);

    QSqlQuery q = createQuery(typeInformation,whereClause,map);
    if(q.first())
        return mapToObject(typeInformation,q,q.record());
    return NULL;
}

bool QxtOrmManager::objectExists(const QxtOrmType *typeInformation, const int id)
{
    QString query="Select "+typeInformation->id().columnName()+" from "+typeInformation->tableName()
            +" where "+typeInformation->id().columnName()+"=:id;";

    QSqlQuery q(this->connection());
    q.prepare(query);
    q.bindValue(":id",id);
    if(q.exec()){
       return q.next();
    }
#ifdef DEBUG
    qDebug()<<"Maybe error: "<<q.lastError().databaseText()<<" "<<q.lastError().driverText();
#endif
    return false;
}
