/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#include "qxtormtype.h"
#include "qxtormtype_p.h"
#include "qxtormproperty.h"
#include "qxtormobject.h"

#include <QDebug>

const QxtOrmProperty *m_staticEmptyProperty = NULL;
const QxtOrmRelation *m_staticEmptyRelation = NULL;

QxtOrmType::QxtOrmType(QxtOrmTypePrivate *dPtr)
{
    d_ptr = QSharedPointer<QxtOrmTypePrivate>(dPtr);
}

QxtOrmType::QxtOrmType(const QxtOrmType &other)
{
    //we will never change the private data so we can just share it
    d_ptr = other.d_ptr;
}

QxtOrmType::~QxtOrmType()
{
}

QxtOrmType &QxtOrmType::operator =(const QxtOrmType &other)
{
    //we will never change the private data so we can just share it
    d_ptr = other.d_ptr;
}

bool QxtOrmType::hasProperty (const char *name) const
{
    if(superClass() != QxtOrmObject::staticTypeInfo()){
        if(superClass()->hasProperty(name))
            return true;
    }
    return d_ptr->m_properties.contains(name);
}

bool QxtOrmType::writeProperty(QxtOrmObject *o, const char *name, const QVariant &value) const
{
    if(superClass() != QxtOrmObject::staticTypeInfo()){
        if(superClass()->writeProperty(o,name,value))
            return true;
    }
    if(d_ptr->m_properties.contains(name)){
        QxtOrmProperty* prop = d_ptr->m_properties.value(name);
        return prop->write(o,value);
    }
    return false;
}

QVariant QxtOrmType::readProperty(const QxtOrmObject *o, const char *name) const
{
    if(superClass() != QxtOrmObject::staticTypeInfo()){
        QVariant val = superClass()->readProperty(o,name);
        if(val.isValid())
            return val;
    }
    if(d_ptr->m_properties.contains(name)){
        QxtOrmProperty* prop = d_ptr->m_properties.value(name);
        return prop->read(o);
    }
    return QVariant();
}

const QxtOrmProperty &QxtOrmType::id() const
{
    return *(d_ptr->m_idProperties.first());
}

//@TODO rewrite api for more safety
const QxtOrmProperty &QxtOrmType::property(const char *name) const
{
    Q_ASSERT_X(d_ptr->m_properties.contains(name),Q_FUNC_INFO,"Property must exist");
    if(!d_ptr->m_properties.contains(name))
        return *m_staticEmptyProperty;

    return *(d_ptr->m_properties[name]);
}

const QxtOrmProperty *QxtOrmType::searchProperty(const char *name) const
{
    if(superClass() != QxtOrmObject::staticTypeInfo()){
        const QxtOrmProperty* prop = superClass()->searchProperty(name);
        if(prop) return prop;
    }

    if(d_ptr->m_properties.contains(name))
        return d_ptr->m_properties[name];

    return NULL;
}

QList<QByteArray> QxtOrmType::properties() const
{
    return d_ptr->m_properties.keys();
}

QList<QByteArray> QxtOrmType::relations() const
{
    return d_ptr->m_relations.keys();
}

QList<QByteArray> QxtOrmType::relations(QxtOrmRelation::Type type) const
{
    return d_ptr->m_relationTypes.value(type);
}

const QxtOrmRelation &QxtOrmType::relation(const char *name) const
{
    Q_ASSERT_X(d_ptr->m_relations.contains(name),Q_FUNC_INFO,"Relation must exist");
    if(!d_ptr->m_relations.contains(name))
        return *m_staticEmptyRelation;
    return *(d_ptr->m_relations[name]);
}

const QxtOrmType *QxtOrmType::superClass() const
{
    return d_ptr->m_superClass;
}

const QxtOrmType *QxtOrmType::baseClass() const
{
    const QxtOrmType* base = this;
    while(base->superClass() != QxtOrmObject::staticTypeInfo())
        base = base->superClass();
    return base;
}

QList<const QxtOrmType *> QxtOrmType::inheritanceList() const
{
    const QxtOrmType* currClass = this;
    QList<const QxtOrmType*> inheritanceList;
    while(currClass != QxtOrmObject::staticTypeInfo()){
        inheritanceList.prepend(currClass);
        currClass = currClass->superClass();
    }
    return inheritanceList;
}

const QByteArray& QxtOrmType::name() const
{
    return d_ptr->m_name;
}

const QByteArray QxtOrmType::metaTypeName() const
{
    return QByteArray(QMetaType::typeName(d_ptr->m_metaId));
}

const QString &QxtOrmType::tableName() const
{
    if(d_ptr->m_tableName.isEmpty())
        return d_ptr->m_stringifiedName;
    return d_ptr->m_tableName;
}

int QxtOrmType::metaTypeId() const
{
    return d_ptr->m_metaId;
}

/**
  * \desc checks if the passed id is valid
  * \note this will always call the base class implementation of the isIdValid check
  */
bool QxtOrmType::isIdValid(const QVariant &id) const
{
    if(superClass() != QxtOrmObject::staticTypeInfo())
        return superClass()->isIdValid(id);
    return (*(d_ptr->checkIdValid))(id);
}
