/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#include "qxtormtransaction.h"
#include "qxtormobject.h"
#include "qxtormtype.h"

#include <QWeakPointer>

QxtOrmTransaction::QxtOrmTransaction() : m_isSavepoint(false)
{
}

QxtOrmTransaction::~QxtOrmTransaction()
{
    cleanup();
}

QxtOrmTransaction::QxtOrmTransaction(const QUuid &savepoint) : m_isSavepoint(true)
  , m_savepointName(savepoint)
{

}

QxtOrmTransaction::QxtOrmTransaction(const QxtOrmTransaction &other)
{
    this->operator =(other);
}

QxtOrmTransaction &QxtOrmTransaction::operator =(const QxtOrmTransaction &other)
{
    if(m_operations.size()){
        cleanup();
    }
    for(int i = 0; i < other.m_operations.size();i++){
        m_operations.append(m_operations.at(i)->clone());
    }
    m_savepointName = other.m_savepointName;
    m_isSavepoint   = other.isSavepoint();
}

void QxtOrmTransaction::addOperation(QxtOrmOperation *operation)
{
    m_operations.append(operation);
}

void QxtOrmTransaction::addOperationsFromNestedTransaction(QxtOrmTransaction *nested)
{
    while(nested->m_operations.size() > 0)
        m_operations.append(nested->m_operations.takeFirst());
}

bool QxtOrmTransaction::isSavepoint() const
{
    return m_isSavepoint;
}

const QUuid &QxtOrmTransaction::savepointName() const
{
    return m_savepointName;
}

bool QxtOrmTransaction::rollback()
{
    for(int i = 0; i < m_operations.size(); i++){
        if(!m_operations[i]->rollback())
            return false;
    }
    cleanup();
    return true;
}

void QxtOrmTransaction::cleanup()
{
    qDeleteAll(m_operations.begin(),m_operations.end());
    m_operations.clear();
}

class QxtOrmSetValueOperationPrivate{
    public:
        QByteArray m_propertyName;
        const QxtOrmType* m_typeInfo;
        QxtOrmObject* m_object;
        QVariant m_oldValue;
};

QxtOrmSetValueOperation::QxtOrmSetValueOperation(const QByteArray &name, const QxtOrmType *t, QxtOrmObject *obj, const QVariant &oldValue)
{
    d_ptr = new QxtOrmSetValueOperationPrivate;
    d_ptr->m_propertyName = name;
    d_ptr->m_typeInfo = t;
    d_ptr->m_object = obj;
    d_ptr->m_oldValue = oldValue;
}

QxtOrmSetValueOperation::QxtOrmSetValueOperation(const QxtOrmSetValueOperation &other)
{
    d_ptr = new QxtOrmSetValueOperationPrivate;
    d_ptr->m_propertyName = other.d_ptr->m_propertyName;
    d_ptr->m_typeInfo = other.d_ptr->m_typeInfo;
    d_ptr->m_object = other.d_ptr->m_object;
    d_ptr->m_oldValue = other.d_ptr->m_oldValue;
}

QxtOrmSetValueOperation::~QxtOrmSetValueOperation()
{
    if(!d_ptr)
        delete d_ptr;
}

bool QxtOrmSetValueOperation::rollback()
{
    QxtOrmObject* ptr = d_ptr->m_object;
    const QxtOrmProperty& prop = d_ptr->m_typeInfo->property(d_ptr->m_propertyName);
    prop.write(ptr,d_ptr->m_oldValue);

    return true;
}

QxtOrmOperation *QxtOrmSetValueOperation::clone()
{
    return new QxtOrmSetValueOperation(*this);
}
