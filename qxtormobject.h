/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#ifndef QXTORMOBJECT_H
#define QXTORMOBJECT_H

#include "qxtorm_global.h"
#include "qxtormtype.h"
#include <QVariant>

template <typename T>
QxtOrmType qxtOrmCreateMapping(){return QxtOrmType(0);}

class QXT_ORM_EXPORT QxtOrmObject
{
    public:
        QxtOrmObject();
        virtual ~QxtOrmObject();
        static const QxtOrmType*  staticTypeInfo();
        virtual const QxtOrmType* typeInfo() const;

    private:
        static const QxtOrmType qxt_staticTypeInfo;
};

#define QXT_ORM(CLASS) \
    private:\
    static const QxtOrmType qxt_staticTypeInfo;\
    public:\
    static const QxtOrmType*  staticTypeInfo();\
    virtual const QxtOrmType* typeInfo() const;\
    friend QxtOrmType qxtOrmCreateMapping<CLASS>();\
    private:

#define QXT_ORM_IMPLEMENT(CLASS) \
    const QxtOrmType* CLASS::staticTypeInfo(){ return &CLASS::qxt_staticTypeInfo; } \
    const QxtOrmType* CLASS::typeInfo() const{return CLASS::staticTypeInfo();}; \
    const QxtOrmType CLASS::qxt_staticTypeInfo = qxtOrmCreateMapping<CLASS>();


#define QXT_DECLARE_ORMTYPE(TYPE) Q_DECLARE_METATYPE(TYPE) \
                                  Q_DECLARE_METATYPE(TYPE*)

QXT_DECLARE_ORMTYPE(QxtOrmObject)

class QDebug;

QDebug operator<<(QDebug out,const QxtOrmObject* data);

#endif // QXTORMOBJECT_H
