#-------------------------------------------------
#
# Project created by QtCreator 2012-01-02T10:28:50
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = qxtorm
TEMPLATE = lib

DEFINES += QXTORM_LIBRARY

SOURCES += qxtormtype.cpp \
    qxtormproperty.cpp \
    qxtormobject.cpp \
    qxtormmanager.cpp \
    qxtormrelation.cpp \
    qxtormtypebuilder.cpp \
    qxtormtransaction.cpp

HEADERS += qxtormtype.h\
        qxtorm_global.h \
    qxtormproperty.h \
    qxtormobject.h \
    qxtormmanager.h \
    qxtormtype_p.h \
    qxtormrelation.h \
    qxtormtypebuilder.h \
    qxtormtransaction.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE9A3BFE4
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = qxtorm.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
