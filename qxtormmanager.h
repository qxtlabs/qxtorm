/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#ifndef QXTORMMANAGER_H
#define QXTORMMANAGER_H

#include <QVariant>
#include "qxtormobject.h"
#include "qxtormtype.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariantMap>
#include <QSharedPointer>

class QSqlRecord;

class QxtOrmManagerPrivate;

class QxtOrmManager
{
    Q_DISABLE_COPY(QxtOrmManager);
    public:
        QxtOrmManager(QSqlDatabase connection);
        ~QxtOrmManager();

        template <typename T>
        QSharedPointer<T> mapToObject (const QString &where,const QVariantMap &values){
            const QxtOrmType *t = T::staticTypeInfo();
            QSqlQuery q = createQuery(t,where,values);
            QSqlRecord r = q.record();
            if(!q.first())
                return QSharedPointer<T>();
            return mapToObject<T>(q,&r);
        }

        template <typename T>
        QList<QSharedPointer<T> > mapToResultSet (const QString &where,const QVariantMap &values){
            const QxtOrmType *t = T::staticTypeInfo();
            QSqlQuery q = createQuery(t,where,values);
            return mapToResultSet<T>(q);
        }

        template <typename T>
        QSharedPointer<T> mapToObject (QSqlQuery &sel,const QSqlRecord* recordInfo = 0){
            const QxtOrmType *t = T::staticTypeInfo();
            const QSqlRecord rec = sel.record();
            QxtOrmObject* data = mapToObject(t,sel, (recordInfo!=0) ? (*recordInfo) : rec);
            return doCast<T>(data);
        }

        template <typename T>
        QList<QSharedPointer<T> > mapToResultSet(QSqlQuery &sel){
            QList<QSharedPointer<T> > result;
            QSqlRecord rec = sel.record();

            while(sel.next()){
               result.append(this->mapToObject<T>(sel,&rec));
            }
            return result;
        }

        template <typename T>
        QSharedPointer<T> objectById ( const int id){
            return doCast<T>(objectById(T::staticTypeInfo(),id));
        }

        template <typename T>
        bool objectExists ( const int id){
            return objectExists(T::staticTypeInfo(),id);
        }

        template <typename T>
        QSharedPointer<T> doCast (QxtOrmObject* data){
            T* realData = dynamic_cast<T*>(data);
            if(!realData){
                if(data) delete data;
                return QSharedPointer<T>();
            }
            return QSharedPointer<T>(realData);
        }

        bool saveObject (QxtOrmObject * obj );
        bool unlinkObject (QxtOrmObject* obj);

        bool transaction( );
        bool rollback   ( );
        bool commit     ( );

        QSqlDatabase connection ( );

        template <typename T>
        QString createSelectPart (){
            return createSelect(T::staticTypeInfo());
        }

        template <typename T>
        QString& appendFieldNames ( QString &query ){
            return appendFieldNames(T::staticTypeInfo(),query);
        }

        template <typename T>
        QString& appendFromPart ( QString &query ){
            return appendFromPart(T::staticTypeInfo(),query);
        }

protected:

        QString  createSelect     (const QxtOrmType *typeInformation);
        QString& appendFieldNames (const QxtOrmType *typeInformation,QString &query);
        QString& appendFromPart   (const QxtOrmType *typeInformation,QString &query);
        QSqlQuery createQuery( const QxtOrmType *typeInformation, const QString &where,const QVariantMap &values );
        QxtOrmObject* mapToObject( const QxtOrmType *typeInformation, const QSqlQuery& sel, const QSqlRecord &rec);
        QxtOrmObject* objectById( const QxtOrmType *typeInformation, const int id);
        bool objectExists(const QxtOrmType *typeInformation, const int id);

    private:
        QxtOrmManagerPrivate* d_ptr;
};

#endif // QXTORMMANAGER_H
