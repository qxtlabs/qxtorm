/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#ifndef QXTORMPROPERTY_H
#define QXTORMPROPERTY_H

#include "qxtorm_global.h"
#include <QVariant>
#include <QString>
#include <QHash>

class QxtOrmObject;

//TODO think about rewriting to function pointers instead of virtuals

class QXT_ORM_EXPORT QxtOrmProperty
{
    public:
        QxtOrmProperty( const char* name );

        bool isId() const;
        void setId (const bool set = true);
        bool isStored() const;
        void setStored (const bool set = true);
        QByteArray name( ) const;
        void setName (const char* name);
        QString columnName( ) const;
        void setColumnName (const QString& name);
        QVariant nullValue () const;
        virtual QVariant read (const QxtOrmObject* object) const = 0;
        virtual bool write(QxtOrmObject* object,const QVariant& value) const = 0;
#if 0
        virtual uint qHash ( const QVariant &var) const = 0;
#endif

        int metaTypeId() const;
protected:
        bool        m_id;
        bool        m_stored;
        QByteArray  m_name;
        QString     m_columnName;
        QVariant    m_nullValue;
        int         m_typeId;
};

template <typename Class, typename PType>
class QxtOrmTypedProperty : public QxtOrmProperty
{
    public:
        QxtOrmTypedProperty( const char* name ,PType Class::* property ) : QxtOrmProperty(name){
            pProperty = property;
            m_typeId  = qMetaTypeId<PType>();
            m_nullValue = QVariant(qMetaTypeId<PType>());
        }

        virtual QVariant read (const QxtOrmObject* object) const{
            const Class* ptr = dynamic_cast<const Class*>(object);
            Q_ASSERT_X(ptr != NULL,"QxtOrmTypedProperty::read","Can not convert BaseObject to TargetType");
            if(ptr != NULL){
                return qVariantFromValue(ptr->*pProperty);
            }
            return QVariant();
        }

        virtual bool write(QxtOrmObject* object,const QVariant& value) const{
            Class* ptr = dynamic_cast<Class*>(object);
            Q_ASSERT_X(ptr != NULL,"QxtOrmTypedProperty::write","Can not convert BaseObject to TargetType");
            if(ptr != NULL){
                if(value.isValid() && !value.isNull()){
                    if(!value.canConvert<PType>())
                        return false;
                }
                ptr->*pProperty = qVariantValue<PType>(value);
                return true;
            }
            return false;
        }
#if 0
        virtual uint qHash ( const QVariant &var ) const{
            return qHash(var.value<PType>());
        }
#endif

    private:
        PType Class::* pProperty;
};

#endif // QXTORMPROPERTY_H
