/****************************************************************************
** Copyright (c) 2006 - 2012, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/
#ifndef QXTORMRELATION_H
#define QXTORMRELATION_H

#include <QVariant>
#include <QSharedPointer>
#include "qxtormproperty.h"

class QxtOrmType;
class QxtOrmRelation
{
    public:

        enum Type{
            OneToOne,
            OneToMany,
            ManyToOne,
            ManyToMany
        };

        QxtOrmRelation( const char* name );

        virtual Type relationType () const = 0;
        virtual bool needsLinkTable () const = 0;
        virtual bool isJoinAble () const = 0;
        const QString& dataTable () const;
        const QString& linkTable () const;
        const QString& linkForeignKey () const;
        const QString& linkKey () const;
        const QString& foreignKey () const;
        const QString& columnName () const;
        const QByteArray& name () const;
        const QxtOrmType* leftTypeInfo() const;
        const QxtOrmType* rightTypeInfo() const;
        bool onDeleteDoCascade () const;
        bool onSaveDoCascade () const;

        //Data access
        virtual QVariant variantFromData ( void* data ) const = 0;
        virtual int count ( QxtOrmObject* obj ) const  = 0;
        virtual void append (QxtOrmObject* obj, QxtOrmObject* data) const  = 0;
        virtual QxtOrmObject* at(QxtOrmObject* obj, const int i ) const  = 0;
        virtual QWeakPointer<QxtOrmObject> weakReference(QxtOrmObject* obj, const int i ) const  = 0;
        virtual void removeAt ( QxtOrmObject* obj, const int i ) const  = 0;
        virtual void clear ( QxtOrmObject* obj ) const  = 0;

    protected:
        const QxtOrmType* m_leftType;
        const QxtOrmType* m_rightType;
        QByteArray m_name;
        QString m_stringifiedName;
        QString m_columnName;
        QString m_foreignKey;
        QString m_linkTableName;
        QString m_linkForeignKey;
        QString m_linkKey;
        bool    m_onSaveDoCascade;
        bool    m_onDeleteDoCascade;

};


template <typename LType, typename RType>
class QxtOrmOneToOneRelation : public QxtOrmRelation{
    public:

        QxtOrmOneToOneRelation( const QString name, QString &foreignKey ,RType  QSharedPointer<LType>::* property ) : QxtOrmProperty(name){
            pProperty = property;
            m_foreignKey = foreignKey;
            m_leftType = LType::staticMetaInfo();
            m_rightType = RType::staticMetaInfo();
        }

        virtual QxtOrmRelation::Type relationType () const{
            return QxtOrmRelation::OneToOne;
        }

        virtual bool needsLinkTable () const{
            return false;
        }

        virtual bool isJoinAble () const{
            return true;
        }

        virtual QVariant variantFromData ( void* data ) const{
            RType* val = reinterpret_cast<RType*>(data);
            return qVariantFromValue<RType>(val);
        }

        virtual int count ( QxtOrmObject* obj ) const {
            if(obj->*pProperty.isNull())
                return 0;
            return 1;
        }

        virtual void append (QxtOrmObject* obj, QxtOrmObject* data) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            RType* cData = dynamic_cast<RType*>(data);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::append","Can not convert BaseObject to TargetType");
            Q_ASSERT_X(cData != NULL,"QxtOrmRelation::append","Can not convert DataObject to TargetType");
            if(ptr != NULL){
                ptr->*pProperty = QSharedPointer<RType>(cData);
            }
        }

        virtual QxtOrmObject* at(QxtOrmObject* obj, const int) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::append","Can not convert BaseObject to TargetType");
            if(ptr != NULL){
                return ptr->*pProperty.data();
            }
            return NULL;
        }

        virtual QWeakPointer<QxtOrmObject> weakReference(QxtOrmObject* obj, const int i ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::append","Can not convert BaseObject to TargetType");
            if(ptr != NULL){
                return ptr->*pProperty.toWeakRef();
            }
            return QWeakPointer<QxtOrmObject>();
        }

        virtual void removeAt ( QxtOrmObject* obj, const int i ) const {
            if(i == 0)
                clear(obj);
        }

        virtual void clear ( QxtOrmObject* obj ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::append","Can not convert BaseObject to TargetType");
            if(ptr != NULL){
                ptr->*pProperty.clear();
            }
        }

    protected:
        RType QSharedPointer<LType>::* pProperty;
};

template <typename LType, typename RType>
class QxtOrmManyToOneRelation : public QxtOrmOneToOneRelation<LType,RType>{
    public:
        QxtOrmManyToOneRelation( const QString name, QString &foreignKey ,RType  QSharedPointer<LType>::* property )
            : QxtOrmOneToOneRelation<LType,RType>(name,foreignKey,property){
        }

        virtual QxtOrmRelation::Type relationType () const{
            return QxtOrmRelation::ManyToOne;
        }
};


template <typename LType,typename RType, template<typename RType> class Container = QList>
class QxtOrmOneToManyRelation : public QxtOrmRelation{
    public:
        QxtOrmOneToManyRelation( const char* name, QString &foreignKey ,RType Container<LType>::* property )
            : QxtOrmRelation(name){
            m_foreignKey = foreignKey;

        }

        virtual QxtOrmRelation::Type relationType () const{
            return QxtOrmRelation::OneToMany;
        }

        virtual bool needsLinkTable () const{
            return false;
        }

        virtual bool isJoinAble () const{
            return false;
        }

        virtual QVariant variantFromData ( void* data ) const{
            RType* val = reinterpret_cast<RType*>(data);
            return qVariantFromValue<RType>(val);
        }

        virtual int count ( QxtOrmObject* obj ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::count","Can not convert BaseObject to TargetType");
            if(!ptr)
                return 0;
            return ptr->*pProperty.count();
        }

        virtual void append (QxtOrmObject* obj, QxtOrmObject* data) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            QSharedPointer<RType> ptrData = QSharedPointer<RType>(dynamic_cast<RType*>(data));

            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::append","Can not convert BaseObject to TargetType");
            Q_ASSERT_X(!ptrData.isNull(),"QxtOrmRelation::append","Can not convert DataObject to TargetType");

            if(ptr != NULL || ptrData.isNull())
                return;

            ptr->*pProperty.append(ptrData);

        }

        virtual QxtOrmObject* at(QxtOrmObject* obj, const int i ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::at","Can not convert BaseObject to TargetType");
            if(!ptr)
                return NULL;

            return ptr->*pProperty.at(i).data();
        }

        virtual QWeakPointer<QxtOrmObject> weakReference(QxtOrmObject* obj, const int i ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            Q_ASSERT_X(ptr != NULL,"QxtOrmRelation::weakReference","Can not convert BaseObject to TargetType");
            if(!ptr)
                return NULL;

            return ptr->*pProperty.at(i).toWeakRef();
        }

        virtual void removeAt ( QxtOrmObject* obj, const int i ) const {
            LType* ptr = dynamic_cast<LType*>(obj);
            if(!ptr)
                return NULL;

            ptr->*pProperty.removeAt(i);
        }

        virtual void clear ( QxtOrmObject* obj ) const {
            obj->*pProperty.clear();
        }

    protected:
        Container<QSharedPointer<RType> > LType::* pProperty;
};

template <typename LType,typename RType, template<typename RType> class Container = QList>
class QxtOrmManyToManyRelation : public QxtOrmOneToManyRelation<LType,RType,Container>{

    public:
        QxtOrmManyToManyRelation( const QString name, const QString& linkTable, const QString &linkKey,
                                const QString &linkForeignKey , const QString &foreignKey,
                                RType Container<LType>::* property )
            : QxtOrmOneToManyRelation<LType,RType,Container>(name,foreignKey,property)
        {
            this->m_linkTableName = linkTable;
            this->m_linkKey = linkKey;
            this->m_linkForeignKey = linkForeignKey;
        }

        virtual QxtOrmRelation::Type relationType () const{
            return QxtOrmRelation::ManyToMany;
        }

        virtual bool needsLinkTable () const{
            return true;
        }

        virtual bool isJoinAble () const{
            return false;
        }
};



#endif // QXTORMRELATION_H
